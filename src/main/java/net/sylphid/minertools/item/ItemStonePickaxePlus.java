
package net.sylphid.minertools.item;

import net.sylphid.minertools.ElementsMinertoolsMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.Item;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import java.util.Set;
import java.util.HashMap;

@ElementsMinertoolsMod.ModElement.Tag
public class ItemStonePickaxePlus extends ElementsMinertoolsMod.ModElement {
	@GameRegistry.ObjectHolder("minertools:stone_pickaxe_plus")
	public static final Item block = null;
	public ItemStonePickaxePlus(ElementsMinertoolsMod instance) {
		super(instance, 16);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemPickaxe(EnumHelper.addToolMaterial("STONE_PICKAXE_PLUS", 1, 175, 6f, 0f, 2)) {
			{
				this.attackSpeed = -2f;
			}
			public Set<String> getToolClasses(ItemStack stack) {
				HashMap<String, Integer> ret = new HashMap<String, Integer>();
				ret.put("pickaxe", 1);
				return ret.keySet();
			}
		}.setUnlocalizedName("stone_pickaxe_plus").setRegistryName("stone_pickaxe_plus").setCreativeTab(CreativeTabs.TOOLS));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(block, 0, new ModelResourceLocation("minertools:stone_pickaxe_plus", "inventory"));
	}
}
